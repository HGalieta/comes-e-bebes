<?php

function cb_setup_theme() {
  add_theme_support('custom-logo');
  add_theme_support('woocommerce');
  add_theme_support('menus');
  register_nav_menus( [
    'header' => 'Menu do cabeçalho'
  ] );
}

function pratos_loop_shop_per_page(){
  return 8;
};
?>