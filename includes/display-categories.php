<?php 

/**Função para mostrar as categorias**/
function show_categories() {

    $agrs = array(
        'taxonomy' => 'product_cat',
        'orderby' => 'name',
    );

    $categories_list = [];
    // Pego todas as categorias (array)
    $categories = get_categories($agrs);
    
    foreach ($categories as $category) {

        if ($category->name != 'Uncategorized') {

            $id_category = $category->term_id;
            $id_img = get_term_meta($id_category, 'thumbnail_id', true);
            
            // Pego as informações de cada categoria e ponho na lista
            $categories_list[] = [
                'name' => $category->name,
                'id' => $id_category,
                'link' => get_term_link($id_category, 'product_cat'),
                'img' => wp_get_attachment_image_src($id_img, 'slide')[0]
            ];
        };

    };

    global $wp;
    $url = home_url( $wp->request );

    foreach ($categories_list as  $category_displayed) {
        if(str_contains($url, strtolower($category_displayed['name'])))
        {
        ?>
        <div class="category">
            <a href="<?= $category_displayed['link']; ?>">
                <img src="<?= $category_displayed['img'] ?>" alt="Categoria <?= $category_displayed['name'] ?>" class="category_img outline_category">
            </a>

            <div class="category_name">
                <h3><?= $category_displayed['name'] ?></h3>
            </div>
        </div>
        <?php
        } else {
            ?>
            <div class="category">
                <a href="<?= $category_displayed['link']; ?>">
                    <img src="<?= $category_displayed['img'] ?>" alt="Categoria <?= $category_displayed['name'] ?>" class="category_img">
                </a>

                <div class="category_name">
                    <h3><?= $category_displayed['name'] ?></h3>
                </div>
            </div>
            <?php 
        }

    }
}

?>