<?php 

function get_current_category()
{
    global $wp;
    $url = home_url( $wp->request );

    if(str_contains($url, 'japonesa')) {
        ?> <h2 class='shop_category'>COMIDA JAPONESA</h2> <?php 
    } elseif (str_contains($url, 'nordestina')) {
        ?> <h2 class='shop_category'>COMIDA NORDESTINA</h2> <?php 
    } elseif (str_contains($url, 'vegana')) {
        ?> <h2 class='shop_category'>COMIDA VEGANA</h2> <?php 
    } elseif (str_contains($url, 'massas')) {
        ?> <h2 class='shop_category'>MASSAS</h2> <?php 
    }
}

?>