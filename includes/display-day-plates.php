<?php 

/**Função para mostrar as os pratos do dia**/
function show_daily_plates() {
    $products = wc_get_products(array(
        'limit'  => -1, // Todos os produtos
        'status' => 'publish', // Somente os publicados
    ) );
    
    $today_plates_list = [];

    foreach ($products as $product) {
        // Pego as informações de cada produto que tem o dia de hoje no atributo 'dia-da-semana'
        if( str_contains($product->get_attribute('pa_dia-da-semana'), date("l") ) ) {
            $product_id = $product->get_id();
            $product_price = $product->get_price();
            $today_plates_list[] = [
                'name' => $product->name,
                'id' => $product_id,
                'link' => get_permalink($product_id),
                'price'=> wc_price( $product_price, array( 'decimal_separator' => ',' ) ),
                'img' => wp_get_attachment_image_src(get_post_thumbnail_id( $product_id ), 'single-post-thumbnail')[0],             
            ];
        };
    };

    foreach ($today_plates_list as $today_plate) {
        ?>
        <div class="category">
            <a href="<?= $today_plate['link']; ?>">
                <img src="<?= $today_plate['img'] ?>" alt="Prato <?= $today_plate['name'] ?>" class="plate_img">
            </a>

            <div class="category_name plate_name">
                <h3><?= $today_plate['name']; ?></h3>

                <div class="price_cart">
                    <h3 class="price_card"><?= number_format((float)$today_plate['price'], 2, ',', ''); ?></h3>

                    <a href="<?= $today_plate['link']; ?>">
                        <img src="<?= IMAGES_DIR . '/carrinho_produto.png' ?>" alt="Carrinho">
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
}

?>