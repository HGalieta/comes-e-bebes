<?php 

    function format_product($product , $id)
    {
        ?>
        <div class="shop_product">
            <a href="<?= $product->get_permalink(); ?>">
                <img src="<?= wp_get_attachment_image_src(get_post_thumbnail_id( $id ), 'single-post-thumbnail')[0] ?>" alt="" class="plate_img shop_product_image">
            </a>

            <div  class="shop_product_name">
                <h3><?= $product->get_name(); ?></h3>

                <div class="price_cart">
                    <h3 class="price_card"><?= number_format((float)$product->get_price(), 2, ',', '');; ?></h3>

                    <a href="<?= $product->add_to_cart_url(); ?>">
                        <img src="<?= IMAGES_DIR . '/carrinho_produto.png' ?>" alt="Carrinho">
                    </a>
                </div>
            </div>
        </div>
        <?php
    }

?>