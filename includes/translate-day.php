<?php 

function translate_day() 
{
    $day = date('l');

    switch ($day) 
    {
        case 'Sunday':
            $day = 'DOMINGO';
            break;
        case 'Monday':
            $day = 'SEGUNDA';
            break;
        case 'Tuesday':
            $day = 'TERÇA';
            break;
        case 'Wednesday':
            $day = 'QUARTA';
            break;
        case 'Thursday':
            $day = 'QUINTA';
            break;
        case 'Friday':
            $day = 'SEXTA';
            break;
        case 'Saturday':
            $day = 'SÁBADO';
            break;
        
        default:
            $day = 'Erro na tradução do dia';
            break;
    }

    ?> <h2 class="home_plates_types_title" id="day_of_the_week"><?= $day ?></h2> <?php
}

?>