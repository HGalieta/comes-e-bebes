<?php

function cb_enqueue_style() {
  wp_register_style('cb-reset', STYLES_DIR . '/reset.css', [], '1.0.0', false);
  wp_register_style('style', get_stylesheet_uri(), [], '1.0.0', false);
  wp_register_style('cb_header', STYLES_DIR . '/header.css', [], '1.0.0', false);
  wp_register_style('cb_footer', STYLES_DIR . '/footer.css', [], '1.0.0', false);
  wp_register_style('cb_home', STYLES_DIR . '/home.css', [], '1.0.0', false);
  wp_register_style('pratin-slider', STYLES_DIR . '/slider.css', [], '1.0.0', false);
  wp_register_style('cb-shop', STYLES_DIR . '/shop.css', [], '1.0.0', false);
  wp_register_style('cb-cart', STYLES_DIR . '/cart.css', [], '1.0.0', false);
  wp_register_script('enter-button', SCRIPTS_DIR . './enter.js', [], '1.0.0', false);
  wp_register_script('filter-loja', SCRIPTS_DIR . './filter.js', [], '1.0.0', false);

  wp_enqueue_style('cb-reset');
  wp_enqueue_style('style');
  wp_enqueue_style('cb_header');
  wp_enqueue_style('cb_footer');
  wp_enqueue_style('cb_home');
  wp_enqueue_style('pratin-slider');
  wp_enqueue_style('cb-shop');
  wp_enqueue_style('cb-cart');

  wp_enqueue_script('enter-button');
  wp_enqueue_script('filter-loja');
}

?>