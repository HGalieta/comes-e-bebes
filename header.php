<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php bloginfo( 'name' ); ?> | <?php the_title(); ?> </title>
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
  <div class="esquerda">
  <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
    <img src="<?php echo IMAGES_DIR; ?>/logo.png"></a>
    </a>
    <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
     <input type="text" name="s" id="s" class="busca" placeholder="Sashimi">
     <div class="linha-busca"></div>
    </form>
  </div>
  <div class="direita">
    <a href="<?php echo esc_url( home_url( '/shop' ) ); ?>" class="custom-button">Faça um pedido</a>
      <img src="<?php echo IMAGES_DIR; ?>/carrinho.png" class="cart_icon">
    <a href="<?php echo esc_url( home_url( '/my-account' ) ); ?>">
      <img src="<?php echo IMAGES_DIR; ?>/user.png">
    </a>
  </div>
</header>


<section class="fog_background">

</section>

<section class="cart_modal">
  <div class="cart_modal_content">
    <div class="cart_title_exit">
      <h1 class="cart_title">Carrinho</h1>

      <h1 class="cart_exit">x</h1>
    </div>

    <div class="cart_products">
      <?php 
      
      $itens = WC()->cart->get_cart();
      $total = 0;

      if (! WC()->cart->is_empty()) {
          foreach($itens as $item => $values) { 
              $product =  wc_get_product( $values['data']->get_id() );
              $price = $product->get_price();
              $total += $price;
              $image = wp_get_attachment_image_src(get_post_thumbnail_id( $product->get_id() ), 'single-post-thumbnail')[0];  
              $title = $product->name; 
              ?>
                  <div class="cart_product">
                    <div class="cart_img_title">
                      <img src="<?= $image ?>" alt="" class="cart_product_img">
                      <div class="cart_product_info">
                          <h3><?= $title ?></h3>

                          <h3>1</h3>
                    </div>
                      </div>
                      <h3 class="cart_product_price"><?= number_format((float)$price, 2, ',', ''); ?></h3>
                  </div>
              <?php 
              

          } 
      } 

      ?>
    </div>

    <h2 class="cart_total"><?= 'Total do Carrinho: ' . number_format((float)$total, 2, ',', ''); ?></h2>

    <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="button_to_checkout">
	  <?php esc_html_e( 'Comprar', 'woocommerce' ); ?>
</a>
  </div>

  <script>

    function open_menu()
    {
        console.log('clicou');
        var cart = document.querySelector(".cart_modal");
        var fog = document.querySelector(".fog_background");
        cart.style.display = "block";
        fog.style.display = "block";
    }

    function close_menu()
    {
        var cart = document.querySelector(".cart_modal");
        var fog = document.querySelector(".fog_background");
        cart.style.display = "none";
        fog.style.display = "none";
    }

    document.querySelector(".cart_icon").addEventListener('click', open_menu);
    document.querySelector(".cart_exit").addEventListener('click', close_menu);

  </script>
</section>