<?php get_header(); ?>

<?php
    $endereco_completo = get_option('pe_cadastro_endereco') . ', ' . get_option('pe_cadastro_cidade');
    $endereco = urlencode(get_option('pe_cadastro_endereco'));
    $telefone = get_option('pe_cadastro_telefone');
?>

<main>
  <section id="home_title_box">
  
    <h1 class="home_title">Comes&Bebes</h1>
  
    <span class="home_subtitle">O restaurante para todas as fomes</span>
  
  </section>

  <section class="home_content_box">
    <h2 class="home_content_title">Conheça Nossa Loja</h2>

    <div class="home_plates_types_div">
      <h2 class="home_plates_types_title">Tipos de pratos principais</h2>
      <section class="home_plates_types">
        <?php show_categories() ?>
      </section>
    </div>

    <div class="home_plates_today">
      <h2 class="home_plates_types_title">Pratos do dia de hoje</h2>
      <?php translate_day() ?>
      <section class="home_plates_types">
        <?php show_daily_plates() ?>
      </section>
    </div>

    <?php 
      wp_nav_menu(['menu'=> 'veja-outras-opcoes']);
    ?>
  </section>

  <section class="home_footer">
    <h1>VISITE NOSSA LOJA FÍSICA</h1>
    <div class="home_footer_body">
      <div class="map">
        <div style="overflow:hidden;width: 250px;position: relative;">
        <iframe width="250" height="168" src="https://maps.google.com/maps?width=250&amp;height=168&amp;hl=en&amp;q=<?php echo $endereco; ?>;&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>

        <div class="icon_info">
          <img src="<?= IMAGES_DIR . './food_icon.png' ?>" alt="Ícone de talheres">
          <span><?= $endereco_completo ?></span>
        </div>

        <div class="icon_info">
          <img src="<?= IMAGES_DIR . './phone_icon.png' ?>" alt="Ícone de telefone">
          <span><?= $telefone ?></span>
        </div>
      </div>

      <img src="<?= IMAGES_DIR . './amigos_felizes.png'?>" alt="Amigos Felizes">
    </div>
  </section>


</main>



<?php get_footer() ?>

