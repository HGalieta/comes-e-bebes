<?php

// Variáveis
define('ROOT_DIR', get_theme_file_path());
define('IMAGES_DIR', get_template_directory_uri() . '/assets/images');
define('STYLES_DIR', get_template_directory_uri() . '/assets/css');
define('SCRIPTS_DIR', get_template_directory_uri() . '/assets/js');
define('INCLUDES_DIR', ROOT_DIR . '/includes');

// Includes
include_once(INCLUDES_DIR . '/enqueue.php');
include_once(INCLUDES_DIR . '/setup-theme.php');
include_once(INCLUDES_DIR . '/display-categories.php');
include_once(INCLUDES_DIR . '/display-day-plates.php');
include_once(INCLUDES_DIR . '/translate-day.php');
include_once(INCLUDES_DIR . '/get-current-category.php');
include_once(INCLUDES_DIR . '/format-product.php');

// Ganchos
add_action('wp_enqueue_scripts', 'cb_enqueue_style');
add_action('after_setup_theme', 'cb_setup_theme');

add_filter('loop_shop_per_page', 'pratos_loop_shop_per_page');

?>