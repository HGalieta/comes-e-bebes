<?php get_header(); ?>

<main>
    <section class="home_content_box">
        <div class="home_plates_types_div">
            <h2 class="shop_subtitles">SELECIONE UMA CATEGORIA</h2>
            
            <section class="home_plates_types" id="shop_plates_categories">
                <?php show_categories() ?>
            </section>
        </div>
    </section>

    <div class="shop_subtitle_and_category">
        <h2 class="shop_subtitles" id="plates">PRATOS</h2>
        
        <?php get_current_category() ?>
    </div>

    <form action="<?= bloginfo('url') ?>/shop/" method="get">
        <div class="label_input">
            <h4>Buscar por nome: </h4>

            <input type="text" name="s" id="s" class="input_text_search_by_name filter_form">
            <input type="text" name="post_type" id="post_type" value="product" class="hideme filter_form">
        </div>
        
        <div class="select_and_filter">
            <div class="label_select">
                <h4>Ordenar por:</h4>

                <select name="orderby" class="orderby filter_form" aria-label="Pedido da loja">
                    <option value="menu_order" selected="selected"></option>
                    <option value="rating">Ordenar por classificação: menor para maior</option>
                    <option value="rating-desc">Ordenar por classificação: maior para menor</option>
                    <option value="price">Ordenar por preço: menor para maior</option>
                    <option value="price-desc">Ordenar por preço: maior para menor</option>
                </select>
            </div>

            <div class="label_select">
                <h4>Filtro de preço:</h4>

                <div class="filter_inputs">
                    <h4>De:</h4>
                    <input type="number" class="filter_form">
                    <h4>Para:</h4>
                    <input type="number" class="filter_form">
                </div>
            </div>
        </div>
        
        <input type="submit" class="hideme" id="submit_filters_button"></input>
    </form>

    <div class="shop_products_block">
        <?php 
            if(have_posts())
            {
                while (have_posts()) 
                {
                    the_post();
                    $id = get_the_ID();
                    format_product(wc_get_product($id), $id); 
                }
                
            }
        ?>
    </div>
    <?php 
        echo get_the_posts_pagination(array(
            'class' => 'pagination',
            'mid_size' => 2,
            'prev_text' => ('<'),
            'next_text' => ('>'),
        ) );
    ?>
</main>

<?php get_header() ?>