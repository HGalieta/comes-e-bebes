/*
Função feita para submeter os filtros na Loja, sem precisar apertar um botão,
somente apertando enter.
*/

var button = document.getElementById("submit_filters_button");
var textBox = document.getElementsByClassName("filter_form");

textBox.addEventListener("keyup", function (event) {
  
    // Checking if key pressed is ENTER or not
    // if the key pressed is ENTER
    // click listener on button is called
    if (event.key == 'enter') {
        button.click();
        textBox.value = ''
    }
});